from django.db import models

from fontawesome.fields import IconField


class Election(models.Model):
    name = models.CharField(
        max_length=50,
        help_text='The name of the election'
    )

    slug = models.SlugField(
        help_text="The election's slug."
    )

    logo = models.ImageField(
        help_text="The logo of the entity for which this election was held.",
        blank=True
    )

    results = models.URLField(
        blank=True,
        help_text="The election's results URL, if any."
    )

    start_date = models.DateField(
        blank=True,
        help_text='The official date that the election started.'
    )

    end_date = models.DateField(
        blank=True,
        help_text='The official date that the election ended.'
    )

    participants = models.ManyToManyField(
        'commitments_site.Party',
        help_text='A list of participants in this election.'
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['end_date']


class Party(models.Model):
    name = models.CharField(
        unique=True,
        max_length=50,
        help_text='The name of the party.'
    )

    slug = models.SlugField(
        help_text="The party's slug."
    )

    website = models.URLField(
        blank=True,
        help_text='The party\'s website, if any.'
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'parties'


class Commitment(models.Model):
    UNACTIONED = 'UA'
    IMPLEMENTED = 'IM'
    FAILED = 'FA'
    IN_PROGRESS = 'PR'

    STATUS_CHOICES = (
        (UNACTIONED, 'No action taken'),
        (IMPLEMENTED, 'Implemented'),
        (FAILED, 'Failed'),
        (IN_PROGRESS, 'In progress'),
    )

    name = models.CharField(
        max_length=100,
        help_text='A short name for the commitment.',
    )

    slug = models.SlugField(
        help_text="The commitment's slug."
    )

    description = models.TextField(
        help_text='A more in-depth description of the commitment.'
    )

    icon = IconField(
        help_text='An icon to distinguish the commitment.'
    )

    status = models.CharField(
        max_length=2,
        help_text='The status of the commitment',
        choices=STATUS_CHOICES
    )

    status_description = models.TextField(
        blank=True,
        help_text='A commitment-specific description of the current status'
                  ', if any.',
    )

    party = models.ForeignKey(
        'commitments_site.Party',
        help_text='The party that made this commitment.'
    )

    elections = models.ManyToManyField(
        'commitments_site.Election',
        help_text='The election(s) this promise was made at.',
    )

    class Meta:
        unique_together = ['name', 'party']
        ordering = ['name']

    def __str__(self):
        return "{s.name} ({s.party})".format(s=self)
