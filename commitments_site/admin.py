from django.contrib import admin

import models


@admin.register(models.Party)
class PartyAdmin(admin.ModelAdmin):
    list_display = ['name', 'website']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(models.Commitment)
class CommitmentAdmin(admin.ModelAdmin):
    list_display = ['name', 'party', 'get_status_display']
    list_filter = ['elections']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(models.Election)
class ElectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'start_date', 'end_date']
    prepopulated_fields = {'slug': ('name',)}
