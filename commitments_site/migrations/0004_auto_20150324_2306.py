# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0003_auto_20150324_2245'),
    ]

    operations = [
        migrations.AddField(
            model_name='commitment',
            name='slug',
            field=models.SlugField(default='', help_text=b"The commitment's slug."),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='election',
            name='slug',
            field=models.SlugField(default='', help_text=b"The election's slug."),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='party',
            name='slug',
            field=models.SlugField(default='', help_text=b"The party's slug."),
            preserve_default=False,
        ),
    ]
