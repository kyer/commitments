# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0008_auto_20150325_0143'),
    ]

    operations = [
        migrations.AddField(
            model_name='election',
            name='logo',
            field=models.ImageField(help_text=b'The logo of the entity for which this election was held.', upload_to=b'', blank=True),
            preserve_default=True,
        ),
    ]
