# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0002_commitment_status_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='Election',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'The name of the election', max_length=50)),
                ('start_date', models.DateField(help_text=b'The official date that the election started.', blank=True)),
                ('end_date', models.DateField(help_text=b'The official date that the election ended.', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='commitment',
            name='election',
            field=models.ManyToManyField(help_text=b'The election(s) this promise was made at.', to='commitments_site.Election'),
            preserve_default=True,
        ),
    ]
