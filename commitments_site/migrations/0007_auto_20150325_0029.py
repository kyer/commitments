# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0006_election_participants'),
    ]

    operations = [
        migrations.RenameField(
            model_name='commitment',
            old_name='election',
            new_name='elections',
        ),
    ]
