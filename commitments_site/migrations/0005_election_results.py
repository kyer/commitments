# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0004_auto_20150324_2306'),
    ]

    operations = [
        migrations.AddField(
            model_name='election',
            name='results',
            field=models.URLField(help_text=b"The election's results URL, if any.", blank=True),
            preserve_default=True,
        ),
    ]
