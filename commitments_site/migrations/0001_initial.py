# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import fontawesome.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Commitment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'A short name for the commitment.', max_length=100)),
                ('description', models.TextField(help_text=b'A more in-depth description of the commitment.')),
                ('icon', fontawesome.fields.IconField(help_text=b'An icon to distinguish the commitment.', max_length=60, blank=True)),
                ('status', models.CharField(help_text=b'The status of the commitment', max_length=2, choices=[(b'UA', b'No action taken'), (b'IM', b'Implemented'), (b'FA', b'Failed'), (b'PR', b'In progress')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Party',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'The name of the party.', unique=True, max_length=50)),
                ('website', models.URLField(help_text=b"The party's website, if any.", blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='commitment',
            name='party',
            field=models.ForeignKey(help_text=b'The party that made this commitment.', to='commitments_site.Party'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='commitment',
            unique_together=set([('name', 'party')]),
        ),
    ]
