# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='commitment',
            name='status_description',
            field=models.TextField(help_text=b'A commitment-specific description of the current status, if any.', blank=True),
            preserve_default=True,
        ),
    ]
