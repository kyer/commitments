# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0005_election_results'),
    ]

    operations = [
        migrations.AddField(
            model_name='election',
            name='participants',
            field=models.ManyToManyField(help_text=b'A list of participants in this election.', to='commitments_site.Party'),
            preserve_default=True,
        ),
    ]
