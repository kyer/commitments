# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('commitments_site', '0007_auto_20150325_0029'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='commitment',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='election',
            options={'ordering': ['end_date']},
        ),
        migrations.AlterModelOptions(
            name='party',
            options={'ordering': ['name']},
        ),
    ]
