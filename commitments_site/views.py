from django.views import generic
from django import shortcuts
import models


class ElectionListView(generic.ListView):
    model = models.Election
    template_name = 'election_list.html'


class ElectionDetailView(generic.TemplateView):
    model = models.Election
    template_name = 'commitments.html'
    slug_field = 'slug'

    def get_context_data(self, *args, **kwargs):
        c = super(ElectionDetailView, self).get_context_data(*args, **kwargs)
        c['election'] = shortcuts.get_object_or_404(
            models.Election, slug=kwargs['slug'])
        c['parties'] = [{
            'party': p,
            'implemented': models.Commitment.objects.filter(
                party=p,
                elections=c['election'],
                status=models.Commitment.IMPLEMENTED
            ),
            'commitments': models.Commitment.objects.filter(
                party=p,
                elections=c['election']
            ),
        } for p in c['election'].participants.all()]
        return c
