from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

from commitments_site import views


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(
        r'^$',
        views.ElectionListView.as_view(),
        name='elections'
    ),
    url(
        r'^(?P<slug>[^/]+)/$',
        views.ElectionDetailView.as_view(),
        name='commitments'
    ),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
