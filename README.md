# README #

### What is this repository for? ###

commitments is a generic election commitment tracker. It's basic, but it'll do

### How do I get set up? ###

commitments is a django project, so you install it like any other.


```
#!bash

$ git clone https://kyer@bitbucket.org/kyer/commitments.git
$ cd commitments
$ pip install -r requirements.txt
$ cp .env.example .env
$ source .env
$ python manage.py syncdb
$ python manage.py createsuperuser
$ python manage.py runserver  # or gunicorn, or whatever
```

### Who do I talk to? ###

* Kye Russell
* [mailto://me@kye.id.au](mailto://me@kye.id.au)